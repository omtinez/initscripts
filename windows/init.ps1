# To run this script directly from PowerShell:
# Invoke-Expression ((New-Object net.webclient).DownloadString('https://gitlab.com/omtinez/initscripts/-/raw/master/windows/init.ps1'))

# Hardcode the Gitlab username
$USERNAME = "omtinez"

# Initialize profile for user if it doesn't exist
New-Item $profile -Type File -Force

# Download our custom profile and overwrite existing one
Invoke-WebRequest https://gitlab.com/$USERNAME/initscripts/raw/master/windows/profile.ps1 -OutFile $profile

# Install some handy modules from the PowerShell Gallery
Install-Module -Name Set-PsEnv -Confirm:$False -Force

# Install chocolatey package manager and Windows utilities if the platform matches
if ($IsWindows) {
    Set-ExecutionPolicy Bypass -Scope Process -Force
    [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072
    Invoke-Expression ((New-Object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
    cinst -y googlechrome 7zip.install putty.install vscode filezilla treesizefree notepadplusplus.install miniconda3 git.install paint.net nodejs.install
}
