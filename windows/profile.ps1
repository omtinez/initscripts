# Useful aliases
# Add them to the file created by:
# $ New-Item $profile -Type File -Force
function l { Get-ChildItem -Force $args }
function npp { start notepad++ }
function Invoke-Admin { Start-Process $([System.Diagnostics.Process]::GetCurrentProcess().MainModule.FileName) -Verb runAs }