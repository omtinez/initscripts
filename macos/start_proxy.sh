#!/bin/bash
disable_proxy()
{
        networksetup -setsocksfirewallproxystate Wi-Fi off
        echo "SOCKS proxy disabled."
}
trap disable_proxy INT
 
networksetup -setsocksfirewallproxy Wi-Fi 127.0.0.1 1080
networksetup -setsocksfirewallproxystate Wi-Fi on
echo "SOCKS proxy enabled. Starting SSH tunnel..."
ssh -D 1080 -q -C -N $1
