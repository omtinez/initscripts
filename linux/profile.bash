# Aliases
alias ll='ls -halF'
alias l='ll'
alias cd..='cd ..'
alias sudosu='sudo bash --init-file ~/.bashrc'
alias sudovi='sudo vi -u ~/.vimrc'
alias ducks='du -cks * | sort -rn | head'

# Define functions

function ssh_key_gen() {
    mkdir -p ~/.ssh && ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
}
export -f ssh_key_gen

function ssh_key_push() {
    ssh-copy-id -i ~/.ssh/id_rsa $@
}
export -f ssh_key_push

function ssh_key_pull() {
    scp $@:~/.ssh/id_rsa.pub /tmp/$@.pub
    cat /tmp/$@.pub >> ~/.ssh/authorized_keys
    rm /tmp/$@.pub
}
export -f ssh_key_pull

function ssh_pwd_disable() {
    # https://gist.github.com/parente/0227cfbbd8de1ce8ad05
    fakesudo sh -c '\
        grep -q "ChallengeResponseAuthentication" /etc/ssh/sshd_config && \
        sed -i "/^[^#]*ChallengeResponseAuthentication[[:space:]]yes.*/c\ChallengeResponseAuthentication no" /etc/ssh/sshd_config || echo "ChallengeResponseAuthentication no" >> /etc/ssh/sshd_config; \
        grep -q "^[^#]*PasswordAuthentication" /etc/ssh/sshd_config && \
        sed -i "/^[^#]*PasswordAuthentication[[:space:]]yes/c\PasswordAuthentication no" /etc/ssh/sshd_config || echo "PasswordAuthentication no" >> /etc/ssh/sshd_config; \
        service ssh restart'
}
export -f ssh_pwd_disable

function ssh_add_all() {
    mkdir -p ~/.ssh
    grep -slR "PRIVATE" ~/.ssh/ | xargs ssh-add
}
export -f ssh_add_all

function ssh_start_agent() {
    mkdir -p ~/.ssh
    ssh-add -l &>/dev/null
    if [ "$?" == 2 ]; then
        test -r ~/.ssh/agent && \
            eval "$(<~/.ssh/agent)" >/dev/null
    
        ssh-add -l &>/dev/null
        if [ "$?" == 2 ]; then
            (umask 066; ssh-agent > ~/.ssh/agent)
            eval "$(<~/.ssh/agent)" >/dev/null
            ssh_add_all
        fi
    fi
}
export -f ssh_start_agent

function ssh_screen() {
    ssh $@ -t screen -DR
}
export -f ssh_screen

function ssh_tunnel() {
    ssh -D 1080 -q -C -N $@
}
export -f ssh_tunnel

function ssh_bind_port() {
    SRV=$1
    shift
    PORT=$1
    shift
    ssh -L $PORT:127.0.0.1:$PORT $SRV $@
}
export -f ssh_bind_port

function install_node() {
    # Install NodeJS
    curl -fsSL https://deb.nodesource.com/setup_lts.x | fakesudo bash - && apti nodejs
    # Install Yarn
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | fakesudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | fakesudo tee /etc/apt/sources.list.d/yarn.list
    update && apti yarn
}
export -f install_node

function install_chrome() {
    dl https://dl-ssl.google.com/linux/linux_signing_key.pub | fakesudo apt-key add -  && \
        echo "deb [arch=$(dpkg --print-architecture)] http://dl.google.com/linux/chrome/deb/ stable main" | fakesudo tee /etc/apt/sources.list.d/google-chrome.list && \
        update && apti google-chrome-stable
}
export -f install_chrome

function install_vscode() {
    dl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg && \
    fakesudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg && \
    echo "deb [arch=$(dpkg --print-architecture)] https://packages.microsoft.com/repos/vscode stable main" | fakesudo tee -a /etc/apt/sources.list.d/microsoft.list && \
    update && apti libxss1 libasound2 code
}
export -f install_vscode

function install_tailscale() {
    curl -fsSL https://tailscale.com/install.sh | sh
}
export -f install_tailscale

function git_setup() {
    git config --global credential.helper 'cache --timeout=999999999'
    git config --global user.name "owahltinez"
    git config --global user.email "oscar@wahltinez.org"
    git config --global push.default simple
    git config --global core.excludesfile ~/.git/.gitignore
}
export -f git_setup

function git_new_project() {
    if [[ ! $GITLAB_TOKEN ]] ; then echo "Env variable GITLAB_TOKEN has not been set" && return 1; fi
    CURR_DIR=${PWD##*/}
    PROJECT_NAME=${1:-$CURR_DIR}
    if [[ -f ~/.git ]] ; then rm -rfi .git ; fi && \
        curl -H "Content-Type:application/json" https://gitlab.com/api/v4/projects?private_token=$GITLAB_TOKEN -d "{ \"name\": \"$PROJECT_NAME\" }" && \
        git init && \
        git remote add origin "https://oauth2:$GITLAB_TOKEN@gitlab.com/omtinez/$PROJECT_NAME.git"
}
export -f git_new_project

function android_clean() {
     find . -name build -exec rm -rf {} \;
}
export -f android_clean

# Start SSH agent
ssh_start_agent

# Import other scripts / envs
if [[ -f ~/.env ]] ; then source ~/.env ; fi
if [[ -f ~/.bash_aliases ]] ; then source ~/.bash_aliases ; fi

# Paths
export PATH=~/.local/bin:$PATH
