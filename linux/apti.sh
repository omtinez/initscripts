#!/bin/sh
set -xe
fakesudo apt-get -yq --no-install-suggests --no-install-recommends install $@
