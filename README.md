# initscripts
Initialization scripts for linux / windows machines and collection of dotfiles / config files.

# Usage
In an environment that supports bash:
```sh
wget -O - https://gitlab.com/omtinez/initscripts/raw/master/linux/init.sh | sh
```
